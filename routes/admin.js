var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'), //mongo connection
    bodyParser = require('body-parser'), //parses information from POST
    methodOverride = require('method-override'), //used to manipulate POST
    fs = require('fs');

router.use(bodyParser.urlencoded({ extended: true }))
router.use(methodOverride(function(req, res){
      if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
      }
}))

//build the REST operations at the base for blobs
//this will be accessible from http://127.0.0.1:3000/huuble if the default route for / is left unchanged
router.route('/')
    //GET all blobs
    .get(function(req, res, next) {
        //retrieve all blobs from Monogo
        mongoose.model('Node').find({}, function (err, nodes) {
              if (err) {
                  return console.error(err);
              } else {
                  //respond to both HTML and JSON. JSON responses require 'Accept: application/json;' in the Request Header
                  res.format({
                      //HTML response will render the index.jade file in the views/blobs folder. We are also setting "blobs" to be an accessible variable in our jade view
                    html: function(){
                        res.render('admin/index', {
                              title: 'All my Nodes',
                              "nodes" : nodes
                          });
                    },
                    //JSON response will show all blobs in JSON format
                    json: function(){
                        res.json(nodes);
                    }
                });
              }
        });
    })
    //POST a new blob
    .post(function(req, res) {
        // Get values from POST request. These can be done through forms or REST calls. These rely on the "name" attributes for forms

        var name = req.body.name;
        var group = req.body.group;
        var desc = req.body.desc;
        var link = req.body.link;
        var tag = req.body.tag;
        var root = true;

        //call the create function for our database
        mongoose.model('Node').create({
            name : name,
            group : group,
            desc : desc,
            link : link,
            tag : tag,
            root : root
        }, function (err, node) {
              if (err) {
                  res.send("There was a problem adding the information to the database.");
              } else {
                  //Blob has been created
                  console.log('POST creating new node: ' + node);
                  if(req.body.parent){
                    mongoose.model('Node').findById(req.body.parent, function (err, nodeParent) {
                      if(nodeParent.children != null){
                        nodeParent.children.push(node)
                      }else{
                        nodeParent.children = []
                        nodeParent.children.push(node)
                      }
                      nodeParent.save(function (err) {
                        if (err) return handleError(err)
                        console.log('Success!');
                      });
                      // console.log(nodeParent);
                    });

                    node.update({
                      root : false
                    }, function (err, nodeID) {
                      if (err)
                          res.send("There was a problem updating the information to the database: " + err);
                      });

                  }
                  res.format({
                      //HTML response will set the location and redirect back to the home page. You could also create a 'success' page if that's your thing
                    html: function(){
                        // If it worked, set the header so the address bar doesn't still say /adduser
                        res.location("admin");
                        // And forward to success page
                        res.redirect("/admin");
                    },
                    //JSON response will show the newly created blob
                    json: function(){
                        res.json(node);
                    }
                });
              }
        })
    });

router.get('/add', function(req, res) {
  var optionsChildren = "<option value=''>Nobody</option>"
    mongoose.model('Node').find().exec(function(err, nodesAll) {
             nodesAll.reverse()
             nodesAll.forEach(function(node){
                 optionsChildren += "<option value='"+node._id+"'>"+node.name+"</option>"
              });
             res.render('admin/add', { title: 'Add New Node', nodes: optionsChildren});
          });
});

router.param('id', function(req, res, next, id) {
    //console.log('validating ' + id + ' exists');
    //find the ID in the Database
    mongoose.model('Node').findById(id, function (err, node) {
        //if it isn't found, we are going to repond with 404
        if (err) {
            console.log(id + ' was not found');
            res.status(404)
            var err = new Error('Not Found');
            err.status = 404;
            res.format({
                html: function(){
                    next(err);
                 },
                json: function(){
                       res.json({message : err.status  + ' ' + err});
                 }
            });
        //if it is found we continue on
        } else {
            //uncomment this next line if you want to see every JSON document response for every GET/PUT/DELETE call
            //console.log(blob);
            // once validation is done save the new item in the req
            req.id = id;
            // go to the next thing
            next();
        }
    });
});

  //GET the individual blob by Mongo ID
router.get('/edit/:id', function(req, res) {
    //search for the blob within Mongo
    mongoose.model('Node').findById(req.id, function (err, node) {
        if (err) {
            console.log('GET Error: There was a problem retrieving: ' + err);
        } else {
            //Return the blob
            console.log('GET Retrieving ID: ' + node._id);
            var optionsChildren = "<option value=''>Nobody</option>"
            mongoose.model('Node').find().exec(function(err, nodesAll) {
                     nodesAll.reverse()
                     var idParent
                     mongoose.model('Node').findById(node._id, function (err, nodeParent) {
                       if(nodeParent){
                         idParent = nodeParent._id
                       }
                     });
                     nodesAll.forEach(function(item){
                         optionsChildren += "<option value='"+item._id+"'"
                         if(item._id == idParent)
                            optionsChildren += " selected='selected'"
                         optionsChildren += ">"+item.name+"</option>"
                      });


                      res.render('admin/edit', {
                         title: 'Edit node ' + node.title,
                         "node" : node,
                         "optionsChildren":optionsChildren,
                         "idParent": idParent
                     });
              });

        }
    });
});

//PUT to update a blob by ID
router.post('/edit/:id', function(req, res) {
    // Get our REST or form values. These rely on the "name" attributes
    var name = req.body.name;
    var group = req.body.group
    var desc = req.body.desc;
    var link = req.body.link;
    var tag = req.body.tag;

   //find the document by ID
        mongoose.model('Node').findById(req.id, function (err, node) {
            //update it
            node.update({
              name : name,
              group : group,
              desc : desc,
              link : link,
              tag : tag
            }, function (err, nodeID) {
              if (err) {
                  res.send("There was a problem updating the information to the database: " + err);
              }
              else {

                if(req.body.parent){
                  node.update({
                    root : false
                  }, function (err, nodeID) {
                    if (err)
                        res.send("There was a problem updating the information to the database: " + err);
                    });

                  mongoose.model('Node').findById(req.body.parent, function (err, nodeParent) {
                    if(nodeParent){
                      if(nodeParent.children != null){
                        nodeParent.children.push(node)
                      }else{
                        nodeParent.children = []
                        nodeParent.children.push(node)
                      }
                    nodeParent.save(function (err) {
                      if (err) return handleError(err)
                      console.log('Success!');
                    });
                  }
                    // console.log(nodeParent);
                  });

                  mongoose.model('Node').findOne({'children': node}, function(err, oldNodeParent){
                      if(err) return err
                      if(oldNodeParent){
                        oldNodeParent.children.remove(node)
                        oldNodeParent.save(function (err) {
                          if (err) return err
                          console.log('Success!');
                        });
                      }
                  });
                }
                      //HTML responds by going back to the page or you can be fancy and create a new view that shows a success page.
                      res.format({
                          html: function(){
                               res.redirect("/admin");
                         },
                         //JSON responds showing the updated values
                        json: function(){
                               res.json(node);
                         }
                      });
               }
            })
        });
});

function insertChildrens(childrens,nameParent){
  for(i in childrens){
    mongoose.model('Node').create({
        name : childrens[i].name,
        group : childrens[i].group,
        desc : childrens[i].desc,
        link : childrens[i].link,
        tag : childrens[i].tag,
        root : false,
        children: []
    }, function (err, node) {

      node.save(function (err) {
        if (err){console.log("error save : "+node.name+" => "+nameParent+err);return err;}
        mongoose.model('Node').findOne({name: nameParent}, function (err, nodeParent) {
          if (err){return err;}
          if(nodeParent){
            if(nodeParent.children == null)
              nodeParent.children = []
            nodeParent.children.push(node)
            nodeParent.save(function (err) {
              if (err){console.log("error save : "+node.name+" => "+nameParent+err);return err;}
              console.log('Success!');
            });
        }

          // console.log(nodeParent);
        });
      });

})
    if(childrens[i].children)
      insertChildrens(childrens[i].children,childrens[i].name)
  }
  return true
}

//import arbre
router.get('/import', function (req, res){


    var obj = JSON.parse(fs.readFileSync('./public/treeJson/pad.json', 'utf8'));
    for(i in obj){
      //console.log(obj[i].children)
      mongoose.model('Node').create({
          name : obj[i].name,
          group : obj[i].group,
          desc : obj[i].desc,
          link : obj[i].link,
          tag : obj[i].tag,
          root : true
      })

      insertChildrens(obj[i].children,obj[i].name)
    }

    res.redirect("/admin");
});

//DELETE Node by ID
router.get('/delete/:id', function (req, res){
    //find blob by ID
    mongoose.model('Node').findById(req.id, function (err, node) {
        if (err) {
            return console.error(err);
        } else {

          mongoose.model('Node').findOne({'children': node}, function(err, oldNodeParent){
              if(err) return err
              if(oldNodeParent){
                oldNodeParent.children.remove(node)
                oldNodeParent.save(function (err) {
                  if (err) return err
                  console.log('Success!');
                });
              }
          });

            if(node.children != null && node.children.length > 0){
              for(i in node.children){
                node.children[i].update({
                  root : true
                }, function (err, nodeID) {
                  if (err)
                      res.send("There was a problem updating the information to the database: " + err);
                  });
              }
            }


            //remove it from Mongo
            node.remove(function (err, node) {
                if (err) {
                    return console.error(err);
                } else {
                    //Returning success messages saying it was deleted
                    console.log('DELETE removing ID: ' + node._id);
                    res.format({
                        //HTML returns us back to the main page, or you can create a success page
                          html: function(){
                               res.redirect("/admin");
                         },
                         //JSON returns the item with the message that is has been deleted
                        json: function(){
                               res.json({message : 'deleted',
                                   item : node
                               });
                         }
                      });
                }
            });
        }
    });
});

module.exports = router;
