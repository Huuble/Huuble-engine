var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');

mongoose.Promise = global.Promise;

/* GET home page. */
router.get('/', function(req, res, next) {
  var optionsChildren = "<option value=''>Nobody</option>"

    mongoose.model('Node').find().exec(function(err, nodesAll) {

             nodesAll.reverse()
             nodesAll.forEach(function(node){
                 optionsChildren += "<option value='"+node._id+"'>"+node.name+"</option>"
              });

              var queryClient = null
              if(req.query.search)
                queryClient = req.query.search

              res.render('index',{dialog : 'none', optionsChildren: optionsChildren,queryClient: queryClient});
          });


});

//PUT to update a blob by ID
router.post('/add', function(req, res) {

    // Get our REST or form values. These rely on the "name" attributes
    var name = req.body.name;
    var group = req.body.group
    var desc = req.body.desc;
    var link = req.body.link;
    var tag = req.body.tag;
    var root = true;

    //call the create function for our database
    mongoose.model('Node').create({
        name : name,
        group : group,
        desc : desc,
        link : link,
        tag : tag,
        root : root
    }, function (err, node) {
          if (err) {
              res.send("There was a problem adding the information to the database.");
          } else {
              //Blob has been created
              console.log('POST creating new node: ' + node);
              if(req.body.parent){
                mongoose.model('Node').findById(req.body.parent, function (err, nodeParent) {
                  if(nodeParent.children != null){
                    nodeParent.children.push(node)
                  }else{
                    nodeParent.children = []
                    nodeParent.children.push(node)
                  }
                  nodeParent.save(function (err) {
                    if (err) return handleError(err)
                    console.log('Success!');
                  });
                  // console.log(nodeParent);
                });

                node.update({
                  root : false
                }, function (err, nodeID) {
                  if (err)
                      res.send("There was a problem updating the information to the database: " + err);
                  });

              }
                      //HTML responds by going back to the page or you can be fancy and create a new view that shows a success page.
                      res.format({
                          html: function(){
                               res.redirect("/");
                         },
                         //JSON responds showing the updated values
                        json: function(){
                               res.json(node);
                         }
                      });
               }
            })
        });

        router.param('id', function(req, res, next, id) {
            //console.log('validating ' + id + ' exists');
            //find the ID in the Database
            mongoose.model('Node').findById(id, function (err, node) {
                //if it isn't found, we are going to repond with 404
                if (err) {
                    console.log(id + ' was not found');
                    res.status(404)
                    var err = new Error('Not Found');
                    err.status = 404;
                    res.format({
                        html: function(){
                            next(err);
                         },
                        json: function(){
                               res.json({message : err.status  + ' ' + err});
                         }
                    });
                //if it is found we continue on
                } else {
                    //uncomment this next line if you want to see every JSON document response for every GET/PUT/DELETE call
                    //console.log(blob);
                    // once validation is done save the new item in the req
                    req.id = id;
                    // go to the next thing
                    next();
                }
            });
        });

//PUT to update a blob by ID
router.post('/edit/:id', function(req, res) {
    // Get our REST or form values. These rely on the "name" attributes
    var name = req.body.name;
    var group = req.body.group
    var desc = req.body.desc;
    var link = req.body.link;
    var tag = req.body.tag;

   //find the document by ID
        mongoose.model('Node').findById(req.id, function (err, node) {
            if (err) return err
            //update it
            node.update({
              name : name,
              group : group,
              desc : desc,
              link : link,
              tag : tag
            }, function (err, nodeID) {
              if (err) {
                  res.send("There was a problem updating the information to the database: " + err);
              }
              else {

                if(req.body.changeParent){
                  node.update({
                    root : false
                  }, function (err, nodeID) {
                    if (err)
                        res.send("There was a problem updating the information to the database: " + err);
                    });

                  mongoose.model('Node').findById(req.body.parent, function (err, nodeParent) {
                    if(nodeParent){
                      if(nodeParent.children != null){
                        nodeParent.children.push(node)
                      }else{
                        nodeParent.children = []
                        nodeParent.children.push(node)
                      }
                    nodeParent.save(function (err) {
                      if (err) return err
                      console.log('Success!');
                    });
                  }
                    // console.log(nodeParent);
                  });

                  mongoose.model('Node').findOne({'children': node}, function(err, oldNodeParent){
                      if(err) return err
                      if(oldNodeParent){
                        oldNodeParent.children.remove(node)
                        oldNodeParent.save(function (err) {
                          if (err) return err
                          console.log('Success!');
                        });
                      }
                  });
                }
                      //HTML responds by going back to the page or you can be fancy and create a new view that shows a success page.
                      res.format({
                          html: function(){
                               res.redirect("/");
                         },
                         //JSON responds showing the updated values
                        json: function(){
                               res.json(node);
                         }
                      });
               }
            })
        });
});

router.get('/delete/:id', function (req, res){
    //find blob by ID
    mongoose.model('Node').findById(req.id, function (err, node) {
        if (err) {
            return console.error(err);
        } else {

          mongoose.model('Node').findOne({'children': node}, function(err, oldNodeParent){
              if(err) return err
              if(oldNodeParent){
                oldNodeParent.children.remove(node)
                oldNodeParent.save(function (err) {
                  if (err) return err
                  console.log('Success!');
                });
              }
          });

          if(node.children != null && node.children.length > 0){
            for(i in node.children){
              if(node.children[i]._id !== "undefined"){
                mongoose.model('Node').findById(node.children[i]._id, function (err, node) {
                    if (err) {
                        return console.error(err);
                    } else {
                      if(node){
                    node.update({
                      root : true
                    }, function (err, nodeID) {
                      if (err)
                          res.send("There was a problem updating the information to the database: " + err);
                      });
                    }
                  }
                  })
                }
              }
            }


            //remove it from Mongo
            node.remove(function (err, node) {
                if (err) {
                    return console.error(err);
                } else {
                    //Returning success messages saying it was deleted
                    console.log('DELETE removing ID: ' + node._id);
                    res.format({
                        //HTML returns us back to the main page, or you can create a success page
                          html: function(){
                               res.redirect("/");
                         },
                         //JSON returns the item with the message that is has been deleted
                        json: function(){
                               res.json({message : 'deleted',
                                   item : node
                               });
                         }
                      });
                }
            });
        }
    });
});

var tabNodeIds = []

function deleteDoublon(tabNode,tab,results){
  if(Array.isArray(tab)){
    for(i in tab){
      if(i == "toBSON")break
      if(typeof tab[i]._id !== "undefined" && tabNodeIds.indexOf(tab[i]._id.toString()) == -1) {
        tabNodeIds.push(tab[i]._id.toString())
        if(Array.isArray(tab[i].children) && tab[i].children.length > 0){
          tab[i].children.sort(function (a, b) {
            return b.children.length - a.children.length;
          });
          console.log("----------------   CHILDREN   ----------------------")
          console.log(tab[i].children)
          console.log("----------------   CHILDREN   ----------------------")
          deleteDoublon(tabNode,tab[i].children,results)
        }
      }else{
        if(typeof tab[i]._id !== "undefined"){
          console.log("----------------   SUPPRIMEEE   ----------------------")
          console.log(tab[i])
          delete results[results.indexOf(tab[i])]
          console.log("----------------   SUPPRIMEEE   ----------------------")
        }
      }
    }
  }
  return results
}

/* GET json page. */
router.get('/json', function(req, res, next) {
  var queryArray = [];

  queryArray.push({
    "root": true
  })

  if(req.query.search) {
    var inputClient = {$regex: new RegExp(req.query.search), $options: 'i' }
    queryArray = []
    for (var property in mongoose.model('Node').schema.paths) {
      if (mongoose.model('Node').schema.paths.hasOwnProperty(property)  && mongoose.model('Node').schema.paths[property]["instance"] === "String") {

          var object = {}
           object[property]=inputClient
        queryArray.push(object)
      }
    }
    //
  }

  var query = {
    $or: queryArray
  };

  var rootHuuble = {
    "name": "Search Huuble",
    "id": "root",
    "value": req.query.search
  };

  //var nodesIds = mongoose.model('Node').find().distinct('_id', nodesIds){_id:{$in: nodesIds}}

  var callback = mongoose.model('Node').find(query).exec(callback)

  callback.then(function(v) {
    v.sort(function (a, b) {
      return b.children.length - a.children.length;
    });
    var results = v
    tabNodeIds = []
    console.log(results)
    results = deleteDoublon(v,v,results)
    console.log(tabNodeIds)
    rootHuuble["children"] = results.filter(function(val){
    if( val == '' || val == NaN || val == undefined || val == null ){
        return false;
    }
    return true;
});

    res.json(rootHuuble);
  });



});

module.exports = router;
