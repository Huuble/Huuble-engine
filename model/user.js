var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var userSchema = new mongoose.Schema({
  "email": { type: String, required: true, unique: true, trim: true },
  "username": { type: String, required: true, unique: true },
  "name": {
    "first": String,
    "last": String
  },
  "password": { type: String, required: true },
  "created_at": { type: Date, default: Date.now },
  "updated_at": Date
})

userSchema.pre("save", function (next) {
  const currentDate = new Date
  this.updated_at = currentDate.now
  next()
})

UserSchema.virtual("password_confirmation").get(function() {
    return this.pw_conf;
}).set(function(value) {
    this.pw_conf = value;
});

UserSchema.path("username").required(true);
UserSchema.path("password").required(true);

UserSchema.pre("save", true, function(next, done) {
    var self = this;
    mongoose.models["User"].findOne({username: self.username}, function(err, user) {
        if(err) {
            done(err);
        } else if(user) {
            self.invalidate("username", "username must be unique");
            done(new Error("username must be unique"));
        } else {
            done();
        }
    });
    next();
});

UserSchema.pre("save",function(next) {
    if(this.pw_conf !== this.password) {
        next(new Error("Must specify the password confirmation"));
    }
    else {
        next();
    }
});

module.exports = mongoose.model("User",UserSchema);
