var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var nodeSchema = new mongoose.Schema({
  name: String,
  group: { type: Number, min: 1, max: 20 },
  desc: String,
  link: String,
  tag: String,
  root: Boolean,
  created_at: Date,
  updated_at: Date,
  children: [{ type: Schema.ObjectId, ref: 'Node' }]
});

var autoPopulateChildren = function(next) {
    this.populate('children');
    next();
};

nodeSchema
.pre('findOne', autoPopulateChildren)
.pre('find', autoPopulateChildren)
.pre('distinct', autoPopulateChildren)
.pre('save', function (next) {
  if(typeof this.children !== "undefined"){
    //this.children = null
    console.log(this.children)
  }
  if(!this.created_at)
    this.created_at = new Date();

  const currentDate = new Date
  this.updated_at = currentDate.now
  
  next();
});


var Node = mongoose.model('Node', nodeSchema);
