
var timer;

var svg = d3.select("svg")
          .call(d3.zoom().on("zoom", function () {
            svg.attr("transform", "translate(" + d3.event.transform.x + " " + d3.event.transform.y + ") scale(" + d3.event.transform.k + ")");
          })).append("g").attr("class",".transform_Root");


    width = svg.attr("width");
    height = svg.attr("height");

var link = svg.selectAll(".link");
var  node = svg.selectAll(".node");


var urlData = "http://localhost:5432/json"
if($("#search").val() != "")
  urlData += "?search="+$("#search").val()

d3.json(urlData,function(error,data){
    if(error) throw error;
      root = d3.hierarchy(data);
      link = link.data(root.links());
      node = node.data(root.descendants());
      run();
});

// Setting up the simulation
var simulation = d3.forceSimulation()
    // add linking force (empty for now)
    .force("link", d3.forceLink())
    // add repelling charge, defaults to -30
    .force("charge", d3.forceManyBody())
    // // add collide force
    .force("collide",d3.forceCollide(40))
    // add force towards center
    .force("center", d3.forceCenter(width / 2, height / 2))
    // call ticked for every simulation tick
    .on("tick", ticked);


function run() {
    // Add nodes to the simulation
    simulation
        .nodes(root.descendants());

    // Create SVG links

    link = link.enter()
        .append("line")
        .attr("class", "link")
        .attr("stroke-width", "2px")
        .attr("stroke", "#ddd")
        .attr("x1", function(d) {
            return d.source.x;
        })
        .attr("y1", function(d) {
            return d.source.y;
        })
        .attr("x2", function(d) {
            return d.target.x;
        })
        .attr("y2", function(d) {
            return d.target.y;
        })

        .merge(link);


    link.exit().remove();

    // Select nodes

    node = node.enter()
        .append("g")
        .attr("class", "node")
        .on('contextmenu', d3.contextMenu(menu))
        .on('dblclick', doubleClick)
        .on('mouseover', mouseOver)
        .on('mouseout', mouseOut)
        .call(d3.drag()
            .on("start", dragstarted)
            .on("drag", dragged)
            .on("end", dragended));

    // Add node SVG circle

    function rayonNode(d) {
        var ndS = nbChildren(d);
        return ndS*0.7 > 5 ? ndS*0.7 : 5
    }

    // Add node SVG circle
    node.append("circle")
        .attr("r", rayonNode)
        .style("fill", color)
        .style("stroke", colorStroke);

    // Add node SVG text


            var elmCollide = node.append("rect")
                .attr("class","collider")
                .attr("width", 110)
                .attr("height", 30)
                .attr("rx", 5)
                .attr("ry", 5);



        node.append("foreignObject").attr("class","text").attr("width", 110).attr("height", 30).append("xhtml:div").style("color", "white")

            .html(function(d) {
                return ("<div style ='display:flex;width:110px;height:30px;text-align:center;align-items: center;justify-content: center;'>"+d.data.name+"</div>");
            })

        .merge(node);

    node.exit().remove();
    // Add link constraints to the simulation

    simulation.force("link")
        .links(root.links())
        // .distance(60)
         .strength(1)
        .iterations(1)
        .distance(60);

    simulation.force("charge")
        .strength(-500);
    simulation.force("collide")
        .iterations(10);

}
