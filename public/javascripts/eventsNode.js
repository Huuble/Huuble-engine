// Function for updating node and link SVG positions
function ticked() {
    link.attr("x1", function(d) {
            return d.source.x;
        })
        .attr("y1", function(d) {
            return d.source.y;
        })
        .attr("x2", function(d) {
            return d.target.x;
        })
        .attr("y2", function(d) {
            return d.target.y;
        });

    node.attr("transform", function (d) {
            return "translate(" + d.x + ", " + d.y + ")";
        });
}

function getWidthOfText(txt, fontname, fontsize){
  // Create a dummy canvas (render invisible with css)
  var c=document.createElement('canvas');
  // Get the context of the dummy canvas
  var ctx=c.getContext('2d');
  // Set the context.font to the font that you are using
  ctx.font = fontsize + fontname;
  // Measure the string
  // !!! <CRUCIAL>  !!!
  console.log(ctx.measureText(txt));
  var length = ctx.measureText(txt).width;
  // !!! </CRUCIAL> !!!
  // Return width
  return length;
}

function dragstarted(d) {
    if (!d3.event.active) simulation.alphaTarget(0.3).restart();
    d.fx = d.x;
    d.fy = d.y;
}

function dragged(d) {
    d.fx = d3.event.x;
    d.fy = d3.event.y;
}

function dragended(d) {
    if (!d3.event.active) simulation.alphaTarget(0);
    d.fx = null;
    d.fy = null;
}


// Color leaf nodes orange, and packages white or blue.
function colorStroke(d) {
    return d._children ? "#3182bd" : d.children ? "#c6dbef" : "rgb(223, 208, 128)";
}

// Color leaf nodes orange, and packages white or blue.
function color(d) {

    var color = "";
    switch(d.data.group){
        case 1:color="#171a29";break;
        case 2:color="#171a29";break;
        case 3:color="#171a29";break;
        case 4:color="#171a29";break;
        case 5:color="#171a29";break;
    }
    return color;
}

function menu(d){
    var menu = [
        {
            title: '<i class=\"fa fa-plus\" aria-hidden=\"true\"></i>', // icon add
            action: function(elm, d, i) {
                addAction(elm,d,i)
                $('#form').attr( "action", "/add");
                $("#add").hide();
                $("#update").show();
                $('#container').dialog( "open" );
                $('#container').show();
            }
        },
        {
            title: '<i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>', // icon edit
            action: function(elm, d, i) {
                editAction(elm,d,i);
                $('#container').dialog( "open" );
                $('#container').show();
            }
        },
        {
            title: '<i class=\"fa fa-trash\" aria-hidden=\"true\"></i>', //icon delete
            action: function(elm, d, i) {
                window.location.href = '/delete/'+d.data._id;
            }
        }
    ]

    return menu
}

function nbChildren(d) {
    var nbChild = 0;
    var tmp = 0

    function recurse(node) {
        if (node.children) tmp = node.children.reduce(function(p, v) { return p + recurse(v); }, 0);
        if (node._children) tmp = node._children.reduce(function(p, v) { return p + recurse(v); }, 0);
        return nbChild++;
    }

    nbChild += recurse(d);
    return nbChild;
}



function displayForm(d) {
    $("#name").val(d.data.name);
    $("#group").val(d.data.group);
    $("#desc").html(d.data.desc);
    $("#link").val(d.data.link);
    $("#tag").val(d.data.tag);
    if(d.parent){
      $("#parent").val(d.parent.data._id);
    }else
        $("#parent").val(d.data._id);
}

/// creation assets

function addAction(elm,d,i){
    $("#name").val("");
    $("#desc").html("");
    $("#link").val("");
    $("#tag").val("");
    $("#parent").val(d.data._id);
    $("#infoBox").empty();
    $('#wrapper').show();
}

function editAction(elm,d,i){
    displayForm(d)
    $('#form').attr( "action", "/edit/"+d.data._id);
    $("#add").hide();
    $("#update").show();
}

function mouseOver(d) {
    d3.select(this)
        .attr("r", function(d) {
            var ndS = nbChildren(d);
            return ndS*2.5 > 5 ? ndS*1.5 : 5
        });
    //.attr("class", "nodesActive");

    $("#rightContent").empty();
    clearTimeout(timer); // on reset le timer pour eviter que les infos ne disparaissent si on reste sur le noeud
    $("#infoBox").html("<h3 id=\"parent\">"+$("#parent option:selected").text()+"</h3><h2 id=\"titre\">"+d.data.name+"</h2><p class=\"motsClefs\">"+d.data.tag+"</p>");
    $("#rightContent").html("<div class=\"tip\"><h4>DOCS</h4><h4>/-INFOS</h4><h4>|</h4></div><h4 id=\"titre\">"+d.data.name+"</h4><p class=\"description\">"+d.data.desc+"</p><p class=\"targetLink\"><a href=\""+d.data.link+"\" target=\"_blank\"></a></p>");
    $("#rightContent").fadeIn(700);

}

function mouseOut(d) {
    d3.select(this)
        .attr("r", function(d) {
            var ndS = nbChildren(d);
            return ndS*2.5 > 5 ? ndS*1.5 : 5
        });
    //.attr("class", "nodesBase");

    clearTimeout(timer); // on reset le timer pour eviter que les event du mouseout ne se cumulent
    timer = setTimeout(eteint,10000);
    function eteint(){
        $("#rightContent").fadeOut( "slow" );
    }
}

function doubleClick(d){
    window.open(d.data.link);
}

/// manage DOM for D3JS
$(document).ready(function() {

    var myItems;
    $("#add").show();
    $("#update").hide();
    $("#delete").hide();

    var dbJson = $('#jsonFile').val()

    $.getJSON(dbJson, function(data) {
        var myItems = data.nodes
        var id;
        var name;
        $.each(myItems, function(key, val) {
            if(key == 0 )
                $("#parent").append(new Option(val.name, val.id, true, true));
            else
                $("#parent").append(new Option(val.name, val.id, false, false));
        })
    });


    $( "#toggleBouton" ).click(function() {
        $("foreignObject.text").fadeToggle(700);
        $("rect.collider").fadeToggle(700);
        $(this).toggleClass("boutonInactive");
        $("div.d3-tip").toggleClass("show");
    });
})
